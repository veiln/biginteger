#include "BigInteger.h"
#include <cstdlib>
#include <ctime>

int main()
{
    srand(time(NULL));
    
    int count = 1000;
    int module = 1000;
    int fSize;
    int sSize;

    BigInteger fNum(1);
    BigInteger sNum(1);
    BigInteger quotient(1);
    BigInteger remainder(1);
    BigInteger zero("0");
    
    for (int i = 0; i < count; ++i) {
        fSize = 1 + rand() % module;
        sSize = 1 + rand() % (module / 2);
            do {
                fNum = BigInteger::getRandom(fSize);
                sNum = BigInteger::getRandom(sSize);
            } while (fNum == zero or sNum == zero);
            quotient = fNum / sNum;
            remainder = fNum % sNum;
        if (!(fNum == sNum * quotient + remainder)) {
            std::cout << "division error:\n"<< i << std::endl;
            std::cout << "\nfirst: " << fNum << std::endl;
            std::cout << "\nsecond: " << sNum << std::endl;
            std::cout << "\nquotient: " << quotient << std::endl;
            std::cout << "\nrem: " << remainder << std::endl;
            break;
        } else if (fNum - remainder != sNum * quotient) {
            break;
            std::cout << "subtraction error:\nfirst Number:" << fNum << "\nsecond Number:" << sNum << std::endl;
        } else if ( remainder >= sNum) {
            std::cout << "remainder greater then divisor" << std::endl;
            break;
        }
    }
    return 0;
}
