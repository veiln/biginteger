#include "BigInteger.h"
#include <algorithm>
#include <cstdlib>
#include <ctime>
#include <stdexcept>
#include <iomanip>
#define print(x) std::cout << #x << " "<< (x) << std::endl;
//BASESIZE = 2

bool BigInteger::isUpperCaseLetter(const char c) const
{
    return c >= 'A' and c <= 'F';
}

bool BigInteger::isDigit(const char c) const
{
    return c >= '0' and c <= '9';
}

bool BigInteger::isLetter(const char c) const
{
    bool isUpper = c >= 'A' and c <= 'F';
    bool isLower = c >= 'a' and c <= 'f';

    return (isUpper or isLower);
}

bool BigInteger::isCorrectHexString(std::string s) const
{
    for (int i = 0; i < s.length(); ++i) {
        if (not isDigit(s[i]) and not isLetter(s[i])) {
            return false;
        }
    }

    return true;
}

std::string BigInteger::toLower(std::string s) const
{
    std::string result(s.length() + 1, ' ');
    const unsigned short caseDifference = 'a' - 'A';

    for (int i = 0; i < s.length(); ++i) {
        result[i] = s[i] + (isUpperCaseLetter(s[i]) ? caseDifference : 0);
    }

    return result;
}

BASE_TYPE BigInteger::hexCharToNum(const char c) const //lowercase
{
    if (isDigit(c)) {
        return c - '0';
    }

    return c - 'a' + 10;
}

BigInteger::BigInteger(): value(1), size{0}
{
}

BigInteger::BigInteger(std::string s)
{
    if (not isCorrectHexString(s)) {
        throw std::invalid_argument("Incorrect string");
    }

    
    std::string str = toLower(s);
    size = s.length() / NIBBLES_IN_BASE_TYPE;

    if (s.length() % NIBBLES_IN_BASE_TYPE != 0) {
        ++size;
    }

    value = std::vector<BASE_TYPE>(size, 0);

    for (int i = 0; i < size ; ++i) {
       for (int j = NIBBLES_IN_BASE_TYPE - 1; j >= 0; --j) {
            if ((i * NIBBLES_IN_BASE_TYPE + j) < s.length()) {
                value[i] += hexCharToNum(str[s.length() - 1 - (i * NIBBLES_IN_BASE_TYPE + j)]);

                if (j != 0) {
                    value[i] <<= NIBBLE;
                }
            }
        }
    }
    normalize();
    size = getRealSize();
}

BigInteger::BigInteger(const BigInteger &n) :size{n.size}, value(n.value)
{
}

BigInteger::BigInteger(const unsigned short n) :size{0}, value(n, 0)
{
}

BigInteger BigInteger::add(const BigInteger &n) const
{
    unsigned int tmp = 0;
    unsigned int carry = 0;

    BigInteger result(size + 1);

    for (int i = 0; i < n.size; ++i) {
        carry = (BASE & tmp) >> BITS_IN_BASE_TYPE;
        tmp = carry + value.at(i) + n.value.at(i);
        result.value.at(i) = tmp & (BASE - 1);
    }

    for (int i = n.size; i < size; ++i) {
        carry = (BASE & tmp) >> BITS_IN_BASE_TYPE;
        tmp = carry + value.at(i);
        result.value.at(i) = tmp & (BASE - 1);
    }

    carry = (BASE & tmp) >> BITS_IN_BASE_TYPE;

    result.value.at(size) = carry;
    result.normalize();
    result.size = result.getRealSize();
    return result;
}

BigInteger BigInteger:: operator + (const BigInteger &n) const
{
    if (size <= n.size) {
        return n.add(*this);
    }

    return add(n);
}

bool BigInteger::operator == (const BigInteger &n) const
{
    return value == n.value;
}

bool BigInteger::operator != (const BigInteger &n) const
{
    return not (*this == n);
}

bool BigInteger::operator < (const BigInteger &n) const
{
    if (size != n.size) {
        return size < n.size;
    }

    for (int i = size - 1; i >= 0; --i) {
        if (value[i] != n.value[i]) {
            return value[i] < n.value[i];
        }
    }

    return false;
}

bool BigInteger::operator > (const BigInteger &n) const
{
    if (size != n.size) {
        return size > n.size;
    }

    for (int i = size - 1; i >= 0; --i) {
        if (value[i] != n.value[i]) {
            return value[i] > n.value[i];
        }
    }

    return false;
}

bool BigInteger::operator <= (const BigInteger &n) const
{
    return (*this < n) or (*this == n);
}

bool BigInteger::operator >= (const BigInteger & n) const
{
    return (*this > n) or (*this == n);
}

BigInteger BigInteger::subtract (const BigInteger &n) const
{
    int carry = 0;
    int tmp = 0;
    BigInteger result(size);

    for (int i = 0; i < n.size; ++i) {
        tmp = value[i] - n.value[i] - carry;
        result.value[i] = tmp;
        carry = tmp < 0; // use signed int tmp to check sign
    }

    for (int i = n.size; i < size; ++i) {
        tmp = value[i] - carry;
        result.value[i] = tmp;
        carry = tmp < 0; // if carry == 0 then value[i] - carry >= 0 and carry == 0 again
    }
    result.normalize();
    result.size = result.getRealSize();

    return result;
}

BigInteger BigInteger::operator - (const BigInteger &n) const
{
    if (*this < n) {
        return n.subtract(*this);
    }

    return subtract(n);
}

BigInteger BigInteger::multiply(const BigInteger &n) const
{
    unsigned int carry = 0;
    int maxSize = size;
    int minSize = n.size;
    unsigned long int tmp = 0;

    BigInteger result(minSize + maxSize);

    for (int i = 0; i < minSize; ++i) {
        if (n.value[i] == 0) {
            result.value[i + maxSize] = 0;
        } else {
            carry = 0;
            for (int j = 0; j < maxSize; ++j) {
                tmp = n.value[i] * value[j] + result.value[i + j] + carry;
                result.value[i + j] = tmp % BASE;
                carry = tmp / BASE;
            }

            result.value[i + maxSize] = carry;
        }
    }
    result.normalize();
    result.size = result.getRealSize();
    return result;
}

BigInteger BigInteger:: operator * (const BigInteger &n) const
{
    const int boundary = 1000;
    if (size > boundary and n.size > boundary) {
        return this->getKaratsubaProduct(n);
    }
    
    if (*this < n) {
        return n.multiply(*this);
    }

    return multiply(n);
}

BigInteger BigInteger::operator * (const int n) const
{
    BigInteger integer = BigInteger(2);

    integer.value[0] = n % BASE;
    integer.value[1] = n / BASE;
    integer.normalize();
    integer.size = integer.getRealSize();

    return *this * integer;
}

BigInteger & BigInteger::operator = (const BigInteger &n)
{
    if (this != &n) {
        value.resize(n.value.size());
        size = n.size;
        value = n.value;
    }

    return *this;
}

BigInteger BigInteger::divide (const BigInteger &n, const bool getRemainder) const
{
    int sizeDifference = size - n.size;
    int normalizingFactor = BASE / (n.value.at(n.size - 1) + 1); // 1 <= d <= b / 2

    BigInteger normalizedDividend = *this * normalizingFactor;
    BigInteger normalizedDivisor = n * normalizingFactor;

    if (normalizedDividend.size == size) {
        ++normalizedDividend.size;
        normalizedDividend.value.resize(normalizedDividend.size);
    }

    unsigned int intQuotient = 0;
    unsigned int intRemainder = 0;
    unsigned int intDividend = 0;
    bool condition = false;

    BigInteger quotient(sizeDifference + 1);

    for (int i = sizeDifference; i >= 0; --i) {
        intDividend = normalizedDividend.value.at(i + n.size - 1) +
                      BASE * normalizedDividend.value.at(i + n.size);
        intQuotient = intDividend / normalizedDivisor.value.at(n.size - 1);
        intRemainder = intDividend % normalizedDivisor.value.at(n.size - 1);

        do {
            condition = intQuotient * (unsigned long int)normalizedDivisor.value.at(n.size - 2) >
                        BASE * intRemainder +
                        (unsigned long int)normalizedDividend.value.at(i + n.size - 2);
            if (intQuotient == BASE or condition) {
                --intQuotient;
                intRemainder += normalizedDivisor.value.at(n.size - 1);
            } else {
                break;
            }
        }
        while (intRemainder < BASE);

        BigInteger croppedDividend(n.size + 2);

        std::copy(normalizedDividend.value.begin() + i,
                  normalizedDividend.value.begin() + i + n.size + 1,
                  croppedDividend.value.begin());

        croppedDividend.size = n.size + 1;

        BigInteger subtrahend = normalizedDivisor * intQuotient;
        int carry = 0;

        if (subtrahend <= croppedDividend) {
            croppedDividend = croppedDividend - subtrahend;

            std::copy(croppedDividend.value.begin(),
                      croppedDividend.value.end(),
                      normalizedDividend.value.begin() + i);

            std::fill(normalizedDividend.value.begin() + croppedDividend.size + i,
                      normalizedDividend.value.begin() + n.size + i + 1,
                      0);

        } else {
            carry = 1; //sub carry

            BigInteger mask(n.size + 2);
            mask.value.at(n.size + 1) = 1;
            mask.size = n.size + 2;

            croppedDividend = mask - (subtrahend - croppedDividend);
        }

        quotient.value.at(i) = intQuotient;

        if (carry == 1) {
            --quotient.value.at(i);
            croppedDividend = croppedDividend + normalizedDivisor;
            --croppedDividend.value.at(croppedDividend.size - 1);

            std::copy(croppedDividend.value.begin(),
                      croppedDividend.value.begin() + n.size + 1,
                      normalizedDividend.value.begin() + i);
        }
    }

    if (not getRemainder) {
        quotient.normalize();
        quotient.size = quotient.getRealSize();
        return quotient;
    } else {
        BigInteger normalizedRemainder(n.size);

        std::copy(normalizedDividend.value.begin(),
                  normalizedDividend.value.begin() + n.size,
                  normalizedRemainder.value.begin());
        normalizedRemainder.normalize();
        normalizedRemainder.size = normalizedRemainder.getRealSize();
        return normalizedRemainder.divideOnBase(normalizingFactor);
    }
}

BigInteger BigInteger::divideOnBase (const unsigned short int divisor) const
{
    if (divisor == 0) {
        throw std::invalid_argument("Division by zero");
    }

    unsigned int tmp = 0;
    unsigned int carry = 0;
    BigInteger result(size);

    for (int i = size - 1; i >= 0; --i) {
        tmp = value[i] + carry * BASE;
        result.value[i] = tmp / divisor;
        carry = tmp % divisor;
    }
    result.normalize();
    result.size = result.getRealSize();
    return result;
}

BigInteger BigInteger::operator / (const BigInteger &n) const
{
    const bool getRemainder = false;

    if (*this < n) {
        return BigInteger("0");
    }

    if (n.size <= 1) {
        return divideOnBase(n.value[0]);
    }

    return divide(n, getRemainder);
}

BigInteger BigInteger::operator % (const BigInteger &n) const
{
    const bool getRemainder = true;

    if (*this < n) {
        return *this;
    }

    if (n.size <= 1) {
        return *this - divideOnBase(n.value[0]) * n;
    }
    
    return getBarrettRemainder(n);
}

BigInteger BigInteger::operator >> (const unsigned short shift) const
{
    if (shift == 0) {
        return *this;
    }

    if (shift < 0) {
        throw std::invalid_argument("Shift value less than zero");
    }

    if (size <= shift) {
        return BigInteger("0");
    }

    BigInteger result(size - shift);
    std::copy(this->value.begin() + shift, this->value.end(), result.value.begin());
    result.size = result.getRealSize();
    return result;
}

BigInteger BigInteger::operator << (const unsigned short shift) const
{
    if (shift == 0) {
        return *this;
    }

    if (shift < 0) {
        throw std::invalid_argument("Shift value less than zero");
    }

    if (*this == BigInteger("0")) {
        return *this;
    }

    BigInteger result(shift + size);

    std::copy(this->value.begin(), this->value.end(), result.value.begin() + shift);
    result.size = result.getRealSize();
    return result;
}


int BigInteger::getRealSize() const // counts only non-zero cells
{
    for (int i = value.size() - 1; i >= 0; --i) {
        if (value[i] != 0) {
            return i + 1;
        }
    }

    return 1;
}

std::ostream & operator << (std::ostream &os, BigInteger const &n)
{
    os << std::hex;
    for (int i = n.value.size() - 1; i >= 0; --i) {
        os << std::setfill('0') << std::setw(4) << n.value[i];
    }

    return os;
}

BigInteger BigInteger::makeRandom(int length) // length in bases;
{
    BigInteger result(length);

    for (int i = 0; i < length; ++i) {
        result.value[i] = (BASE_TYPE)(1 + rand() % BASE);
    }

    while (result.value[length - 1] == 0) {
        result.value[length - 1] = (BASE_TYPE)(1 + rand() % BASE);
    }

    result.normalize();
    result.size = result.getRealSize();
    return result;
}

BigInteger BigInteger::power(const BigInteger &index, const BigInteger &module) const
{
    if (index == BigInteger("0")) {
        return BigInteger("1");
    }

    BigInteger power(*this);
    BigInteger result("1");

    for (auto cell : index.value) {
        for (int i = 0; i < BITS_IN_BASE_TYPE; ++i) {

            if (isBitOn(cell, i)) {
                result = (result * power).getBarrettRemainder(module);
            }

            power = (power * power).getBarrettRemainder(module);
        }

    }
    return result.getBarrettRemainder(module);
}

void BigInteger::normalize()
{
    int nonzero = value.size() - 1;
    for (int i = value.size() - 1; i > 0; --i) {
        if (value[i] == 0) {
            --nonzero;
        } else {
            break;
        }
    }

    for (int i = value.size() - 1; i > nonzero; --i) {
        value.pop_back();
    }
}

BigInteger BigInteger::slice(const int left, const int right) const
{
    if (left > right) {
        return BigInteger("0");
    }

    BigInteger result(right - left + 1);
    std::copy(value.begin() + size - 1 - right,
              value.begin() + size - left,
              result.value.begin());

    result.size = result.getRealSize();
    return result;
}

BigInteger BigInteger::getKaratsubaProduct(const BigInteger &n) const
{
    if (n.size < 250 or size < 250) {
        return *this * n;
    }

    unsigned short middlePoint = 0;

    if (size > n.size) {
        middlePoint = size / 2;
    } else {
        middlePoint = n.size / 2;
    }

    BigInteger head = *this >> middlePoint;
    BigInteger tail;
    
    if (size > middlePoint) {
        tail = slice(size - middlePoint, size - 1);
    } else {
        tail = *this;
    }

    BigInteger nHead = n >> middlePoint;
    BigInteger nTail;

    if (n.size > middlePoint) {
        nTail = n.slice(n.size - middlePoint, n.size - 1);
    } else {
        nTail = n;
    }

    BigInteger headsProduct = head.getKaratsubaProduct(nHead);
    BigInteger tailsProduct = tail.getKaratsubaProduct(nTail);
    BigInteger middleProduct = (head + tail).getKaratsubaProduct(nHead + nTail) - (headsProduct + tailsProduct);

    BigInteger result = (headsProduct << 2 * middlePoint) + (middleProduct << middlePoint) + tailsProduct;
    result.size = result.getRealSize();
    return result;
}

BigInteger BigInteger::getBarrettRemainder(const BigInteger &module) const
{
    if (size > 2 * module.size) {
        return *this % module;
    }
    if (size < module.size) {
        return *this;
    }

    BigInteger base = BigInteger("10000");
    static BigInteger prevModule = BigInteger("0");
    static BigInteger precomputation;

    if (module != prevModule) {
        precomputation = (base << (2 * module.size - 1)) / module;
        prevModule = module;
    }
    BigInteger r1 = capture(module.size);

    BigInteger predictedQuotient = ((*this >> (module.size - 1)) * precomputation) >> (module.size + 1);
    BigInteger tmp = (predictedQuotient * module);
    BigInteger r2 = tmp.capture(module.size);
    BigInteger remainder;
    
    if (r1 >= r2) {
        remainder = r1 - r2;
    } else {
        remainder = (base << module.size) + r1 - r2;
    }

    while (remainder >= module) {
        remainder = remainder - module;
    }
    return remainder;
}

BigInteger BigInteger::capture(const unsigned short moduleSize) const
{
    if (size > moduleSize) {
        return this->slice(size - moduleSize - 1, size - 1);
    }
    return *this;
}

BigInteger BigInteger::rightBitShift(const unsigned short shift) const
{   
    if (shift < 0) {
        throw std::invalid_argument("Shift value less than zero");
    }

    if (*this == BigInteger("0") or shift == 0) {
        return *this;
    }

    const unsigned short baseNumber = shift / BITS_IN_BASE_TYPE;
    
    if (baseNumber >= size) {
        return BigInteger("0");
    }
    
    const unsigned short bitNumber = shift % BITS_IN_BASE_TYPE;

    BigInteger result = *this >> baseNumber;

    if (bitNumber == 0) {
        return result;
    }

    const unsigned short rightmostBitMask = (1 << (bitNumber)) - 1;
    const unsigned short baseShiftSize = BITS_IN_BASE_TYPE - bitNumber;
    unsigned short prevValue = 0;
    unsigned short curValue = 0;
    
    for (int i = result.size - 1; i >= 0; --i) {
        curValue = result.value.at(i) & rightmostBitMask;
        result.value.at(i) >>= bitNumber;
        result.value.at(i) += prevValue << baseShiftSize;
        prevValue = curValue;
    }

    result.normalize();
    result.size = result.getRealSize();
    return result;
}

BigInteger BigInteger::makePrime(const unsigned short primeSize)
{
    BigInteger probablyPrime;
    unsigned short baseNumber = primeSize / BITS_IN_BASE_TYPE;
    unsigned short bitNumber = primeSize % BITS_IN_BASE_TYPE;
    unsigned short mask = (1 << bitNumber) - 1;
    unsigned short orMask = (1 << (bitNumber - 1));
    
    do {

        if (bitNumber != 0) {
            probablyPrime = makeRandom(baseNumber + 1);
            probablyPrime.value[baseNumber] = probablyPrime.value[baseNumber] % mask;
            probablyPrime.value[baseNumber] |= orMask;
        } else {
            probablyPrime = makeRandom(baseNumber);
            probablyPrime.value[baseNumber - 1] |= 0x8000;
        }
        
    } 
    while (probablyPrime.value[0] % 2 == 0 or not probablyPrime.millerRabinPrimalityTest(10));

    return probablyPrime;
}

bool BigInteger::millerRabinPrimalityTest(const unsigned int times) const
{
    BigInteger one = BigInteger("1");
    BigInteger minusOne = *this - one;
    
    unsigned short powerOfTwo;
    
    BigInteger oddFactor = getOddFactor(powerOfTwo);
    BigInteger leftBoundary = BigInteger("2");
    BigInteger rightBoundary = *this - leftBoundary;
    BigInteger a;
    BigInteger b;

    for (int i = 0; i < times; ++i) {
        do {
            a = makeRandom(size) % *this;
        }
        while(leftBoundary > a or a > rightBoundary);
        b = a.power(oddFactor, *this);
        
        if (not(b == one or b == minusOne)) {
            for (int j = 1; j < powerOfTwo && b != minusOne; ++j) {
                b = (b * b) % *this;
                if (b == one) {
                    return false;
                }
            }

            if (b != minusOne) {
                return false;
            }
        }
    }
    
    return true;
}

BigInteger BigInteger::getOddFactor(unsigned short &powerOfTwo) const
{
    if (*this <= BigInteger("1")) {
        throw std::invalid_argument("Can't get odd factor of number less than two");
    }

    BigInteger one = BigInteger("1");
    BigInteger n  = *this - one;
    
    unsigned short val = 0;

    powerOfTwo = 0;
    for (int i = 0; i < n.size; ++i) {
        if (n.value[i] == 0) {
            powerOfTwo += 16;
        } else {
            val = n.value[i];
            break;
        }
    }

    for (int i = 0; i < BITS_IN_BASE_TYPE; ++i) {
        if (isBitOn(val, i)) {
            break;
        } else {
            ++powerOfTwo;
        }
    }
    
    BigInteger result = n.rightBitShift(powerOfTwo); 
    return result;
}

