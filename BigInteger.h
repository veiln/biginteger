#include <iostream>
#include <vector>
#include <string>

typedef unsigned short int BASE_TYPE;

class BigInteger {
public:
    BigInteger();
    BigInteger(std::string s);
    BigInteger(const BigInteger &n);

    bool operator > (const BigInteger &n) const;
    bool operator < (const BigInteger &n) const;
    bool operator == (const BigInteger &n) const;
    bool operator != (const BigInteger &n) const;
    bool operator <= (const BigInteger &n) const;
    bool operator >= (const BigInteger &n) const;

    BigInteger operator + (const BigInteger &n) const;
    BigInteger operator - (const BigInteger &n) const;
    BigInteger operator * (const BigInteger &n) const;
    BigInteger operator * (const int n) const;
    BigInteger operator / (const BigInteger &n) const;
    BigInteger operator % (const BigInteger &n) const;
    BigInteger & operator = (const BigInteger &n);
    BigInteger operator >> (const unsigned short shift) const;
    BigInteger operator << (const unsigned short shift) const;

    BigInteger power(const BigInteger &index, const BigInteger &module) const;

    friend std::ostream & operator << (std::ostream &, BigInteger const &);

    static BigInteger makeRandom(int length);

    BigInteger getKaratsubaProduct (const BigInteger &n) const;
    BigInteger getBarrettRemainder (const BigInteger &module) const;

    void normalize();

    static BigInteger makePrime(const unsigned short primeSize);   
    bool millerRabinPrimalityTest(const unsigned int times) const;
    BigInteger getOddFactor(unsigned short &powerOfTwo) const;
    BigInteger rightBitShift (const unsigned short shift) const;
    BigInteger slice (const int left, const int right) const;
    BigInteger capture(const unsigned short moduleSize) const;
    int getRealSize() const;

private:

    static const int BASE_TYPE_SIZE = sizeof(BASE_TYPE);
    static const int NIBBLES_IN_BASE_TYPE = BASE_TYPE_SIZE * 2;
    static const int NIBBLE = 4;
    static const int BASE = 1 << (NIBBLES_IN_BASE_TYPE * NIBBLE);
    static const int BITS_IN_BASE_TYPE = NIBBLE * NIBBLES_IN_BASE_TYPE;

    std::vector<BASE_TYPE> value;
    unsigned short size;

    BigInteger(const unsigned short n);

    bool isDigit(const char c) const;
    bool isLetter(const char c) const;
    bool isUpperCaseLetter(const char c) const;
    bool isCorrectHexString(std::string s) const;
    std::string toLower(std::string s) const;

    BASE_TYPE hexCharToNum(const char c) const;
    std::string valueToHex() const;

    static bool isBitOn(const unsigned short number, const unsigned short bit);

    BigInteger add (const BigInteger &n) const;
    BigInteger subtract (const BigInteger &n) const;
    BigInteger multiply (const BigInteger &n) const;
    BigInteger divide (const BigInteger &n, const bool getRemainder) const;
    BigInteger divideOnBase (const unsigned short int n) const;
};

inline bool BigInteger::isBitOn(const unsigned short number ,const unsigned short bit)
{
    return (number >> bit) & 1 == 1;
}

