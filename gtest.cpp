#include "BigInteger.cpp"
#include <gtest/gtest.h>
#include <ctime>
#include <cstdlib>

//TEST(division,cycle)
//{
//    srand(time(nullptr));
//    int count = 1000;
//    int module = 1000;
//
//    BigInteger a;
//    BigInteger b;
//    BigInteger c;
//    BigInteger d;
//    BigInteger z("0");
//
//    int fs = 0;
//    int ss = 0;
//
//    for (int i = 0; i < count; ++i) {
//        do{
//            fs = 1 + rand() % module;
//            ss = 1 + rand() % module;
//
//            a = BigInteger::getRandom(fs);
//            b = BigInteger::getRandom(ss);
//        } while (a == z or b == z);
//        c = a / b;
//        d = a % b;
//        ASSERT_EQ(c * b + d, a);
//        ASSERT_EQ(1, d < b);
//        ASSERT_EQ(c * b, a - d);
//    }
//}

//TEST(baseDivision, with_carry)
//{
//    BigInteger a("efffffff");
//    BigInteger b("ffff");
//    BigInteger c("f000");
//    ASSERT_EQ(c, a / b);
//}

//TEST(getPower, cycle)
//{
//    for (int i = 0; i < 1000; ++i) {
//        BigInteger a = BigInteger::getRandom(100);
//        BigInteger m = BigInteger("1b02761f9c6b100f64e1b27a78330195da1f6902cdeaf4c71c71c7"); 
//        ASSERT_EQ(a % m, a.getPower(m, m));
//    }
//}
//
//TEST(getPower, simple2)
//{
//    BigInteger a("ffffffff");
//    BigInteger index("ffff");
//    BigInteger m("2434");
//    BigInteger r("1783");
//    ASSERT_EQ(r, a.getPower(index, m));
//}
//
//TEST(getPower, one_index)
//{
//    BigInteger a("ffffffff");
//    BigInteger index("1");
//    BigInteger m("2434");
//    BigInteger r("3");
//    ASSERT_EQ(r, a.getPower(index, m));
//}
//
//TEST(getPower, zero_index)
//{
//    BigInteger a("ffffffff");
//    BigInteger index("0");
//    BigInteger m("2434");
//    BigInteger r("1");
//    ASSERT_EQ(r, a.getPower(index, m));
//}
//
//TEST(getPower, zero)
//{
//    BigInteger a("0");
//    BigInteger index("ffff");
//    BigInteger m("2434");
//    BigInteger r("0");
//    ASSERT_EQ(r, a.getPower(index, m));
//}
//
//TEST(getPower, simple6)
//{
//    BigInteger a("ffffffff");
//    BigInteger index("2434");
//    BigInteger m("ffff");
//    BigInteger r("0");
//    ASSERT_EQ(r, a.getPower(index, m));
//}
//
//TEST(getPower, simple7)
//{
//    BigInteger a("ffffffff");
//    BigInteger index("1b02761f9c6b100f64e1b27a78330195da1f6902cdeaf4c71c71c7");
//    BigInteger m("1b02761f9c6b100f64e1b27a78330195da1f6902cdeaf4c71c71c7");
//    ASSERT_EQ(a, a.getPower(index, m));
//}
//

//TEST(slice, simple)
//{
//    BigInteger a("1234567890ff");
//    BigInteger b = a.slice(1, 2);
//    BigInteger c("567890ff");
//    ASSERT_EQ(c, b);
//}
//
//
//TEST(slice, one)
//{
//    BigInteger a("1234567890ff");
//    BigInteger b = a.slice(1, 1);
//    BigInteger c("5678");
//    ASSERT_EQ(c, b);
//}
//
//
//TEST(slice, all)
//{
//    BigInteger a("1234567890ff");
//    BigInteger b = a.slice(0, 2);
//    ASSERT_EQ(a, b);
//}
//
//
//TEST(slice, boundary)
//{
//    BigInteger a("1234567890ff");
//    BigInteger b = a.slice(0, 1);
//    BigInteger c("12345678");
//    ASSERT_EQ(c, b);
//}
//
//TEST(put, simple)
//{
//    BigInteger a("1234567890ff");
//    BigInteger b = a.put(1);
//    BigInteger c("1234567890ff0000");
//    ASSERT_EQ(c, b);
//}
//
//
//TEST(put, zero)
//{
//    BigInteger a("1234567890ff");
//    BigInteger b = a.put(0);
//    BigInteger c("1234567890ff0000");
//    ASSERT_EQ(a, b);
//}

//TEST(karatsuba, time)
//{
//    BigInteger a = BigInteger::getRandom(27650);
//    BigInteger b = BigInteger::getRandom(27650);
//    clock_t begin = clock();
//    for(int i = 0; i < 100; ++i) {
//        BigInteger d = a.karatsuba(b);
//    }
//    clock_t karatsubaTime = clock();
//    
//    for(int i = 0; i < 100; ++i) {
//        BigInteger c = a * b;
//    }
//    clock_t simpleTime = clock();
//
//    std::cout <<  "Simple: " << double(simpleTime - karatsubaTime) / CLOCKS_PER_SEC << "\nKaratsuba: " << double(karatsubaTime - begin) / CLOCKS_PER_SEC << std::endl;
//}

//TEST(sub, time)
//{
//    srand(time(NULL));
//    BigInteger a = BigInteger::getRandom(2);
//    BigInteger b = BigInteger::getRandom(2);
//
//    clock_t begin = clock();
//    BigInteger c = a - b;
//    clock_t subTime = clock();
//
//    std::cout << "Sub: " << 10000000 * double(subTime - begin) / CLOCKS_PER_SEC << std::endl;
//}
//TEST(barrett, simple)
//{
//        srand(time(NULL));
//        BigInteger a = BigInteger::getRandom(4000);
//        BigInteger b = BigInteger::getRandom(2000);
//        clock_t begin = clock();
//        for(int i =0; i < 100; ++i) {
//            a.barrett(b);
//        }
//        clock_t barrettTime = clock();
//        
//        for(int i = 0; i < 100; ++i) {
//            a.barrett(b);
//        }
//        clock_t simpleTime = clock();
//    
//        std::cout << "\nBarrett: " << double(simpleTime - barrettTime) / CLOCKS_PER_SEC << std::endl;
//}
//
//TEST(shift1, simple)
//{
//    BigInteger a("abcd12345678dcba43218765");
//    BigInteger b("abcd0000");
//    ASSERT_EQ(a.slice(0, a.getSize() - 1 - 3), a >> 3);
//}


//TEST(rightBitShift, simple)
//{
//    BigInteger a("0");
//    BigInteger b("ffff");
//    ASSERT_EQ(a.rightBitShift(0), a);
//    ASSERT_EQ(a.rightBitShift(1), a);
//    BigInteger expect1("7fff");
//    BigInteger expect16("0");
//    ASSERT_EQ(b.rightBitShift(1), expect1);
//    ASSERT_EQ(b.rightBitShift(16), expect16);
//    BigInteger c("ffffffffff");
//    ASSERT_EQ(c.rightBitShift(1), BigInteger("7fffffffff"));
//    ASSERT_EQ(c.rightBitShift(16), BigInteger("ffffff"));
//    ASSERT_EQ(c.rightBitShift(32), BigInteger("ff"));
//    ASSERT_EQ(c.rightBitShift(14), BigInteger("3ffffff"));
//    ASSERT_EQ(c.rightBitShift(27), BigInteger("1fff"));
//    BigInteger d("123456789");
//    ASSERT_EQ(d.rightBitShift(3), BigInteger("2468acf1"));
//    ASSERT_EQ(d.rightBitShift(17), BigInteger("91a2"));
//}


//TEST(getOddFactor, simple)
//{
//    BigInteger a("10000");
//    BigInteger b("ffff");
//    unsigned short powerOfTwo;
//    ASSERT_EQ(a.getOddFactor(powerOfTwo), b);
//    ASSERT_EQ(powerOfTwo, 0);
//}

TEST(MR, simple)
{
    
    srand(time(NULL));
    //BigInteger a("2cffffffffffffffffffffffffffffff4c000000000000000000000000000000b5");
    //ASSERT_EQ(a.MillerRabinPrimalityTest(2), true);
    for(int i = 1008; i < 1009; ++i) {
        std::cout << i << " : "<< BigInteger::getPrime(i) << std::endl;
    }
}

int main(int argc, char * argv[])
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
